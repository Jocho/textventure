import { defaultCommands} from './default-commands.js';
import {Terminal} from './terminal.js';
import {data, engine} from './engine.js';
import {Flw, Elem} from '../vendor/flwjs/flw.js';

var flw = new Flw('#main');
var screen = new Elem('screen', {
    content: []
});

var input = new Elem('input', {
    inputType: 'text',
    inputValue: '',
    inputPrefix: '>',

    checkInput: (ev, me) => {
        me.props.inputValue = ev.target.value;
        
        if (data.scene.frames !== null) {
            engine.resolveScene(data.scene);
            return;
        }

        me.props.input = ev.target.value.replace(/ +/, ' ');
        
        let updateInput = null;

        switch (ev.keyCode) {
            case 38:
                terminal.historyPointer += terminal.historyPointer < terminal.history.length -1 ? 1 : 0;
                updateInput = typeof terminal.history[terminal.historyPointer] !== 'undefined' ? terminal.history[terminal.historyPointer] : '';
                break;
            case 40:
                terminal.historyPointer -= terminal.historyPointer > -1 ? 1 : 0;
                updateInput = typeof terminal.history[terminal.historyPointer] !== 'undefined' ? terminal.history[terminal.historyPointer] : '';
                break;

            case 13:
                if (me.props.inputValue.length) {
                    terminal.history.unshift(me.props.input);

                    if (terminal.history.length > 20) {
                        terminal.history.pop();
                    }
        
                    terminal.cmd(me.props.input);
                    
                    me.set('inputValue', '');
                }
                break;
        }

        if (null !== updateInput) {
            me.set('inputValue', updateInput);
        }
    }
});

var app = new Elem('app', {
    screen: screen,
    input: input,
    theme: 'green',

    focusInput: (ev, me) => {
        document.querySelector('.input').focus();
    }
});

var terminal = new Terminal(app, defaultCommands);

flw.addElem(app);

flw.run();

engine.setScene('menu');

export {terminal};