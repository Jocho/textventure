class Player {
    constructor(location, inventory) {
        this.location = location;
        this.inventory = inventory;
    }

    getItem(itemIdx) {
        this.inventory.push(itemIdx);
    }

    looseItem(itemIdx) {
        let inventoryIdx = this.inventory.indexOf(itemIdx);

        if (inventoryIdx == -1) {
            return null;
        }
        
        this.inventory.splice(inventoryIdx, 1);
        return null;
    }
}

class Entity {
    constructor(name, desc, longDesc, state) {
        if (typeof desc !== 'undefined' && typeof desc.init === 'undefined') {
            throw new Error('Description has no `init` state defined.');
        }

        if (typeof longDesc !== 'undefined' && typeof longDesc.init === 'undefined') {
            throw new Error('Long description has no `init` state defined.');
        }

        this.name = name;
        this.desc = desc || {init: 'No description.'};
        this.longDesc = longDesc || {init: 'No long description.'};
        this.state = state || 'init';
        this.itemUsed = {};
    }

    getDesc() {
        return typeof this.desc[this.state] !== 'undefined' ? this.desc[this.state] : this.desc['init'];
    }

    getLongDesc() {
        return typeof this.longDesc[this.state] !== 'undefined' ? this.longDesc[this.state] : this.longDesc['init'];
    }

    onItemUsed(itemIdx, state, reaction) {
        this.itemUsed[itemIdx] = {
            state: state || 'init',
            do: reaction || null
        }

        return this;
    }

    useItem(itemIdx) {
        if (typeof this.itemUsed[itemIdx] !== 'undefined') {
            return this.itemUsed[itemIdx];
        }

        return null;
    }
}

class InventorableEntity extends Entity {
    constructor(name, desc, longDesc, state, inventory) {
        super(name, desc, longDesc, state);

        this.inventory = inventory || [];
        this.itemLost = {};
        this.itemGot = {};
    }

    onItemLost(itemIdx, state, reaction) {
        this.itemLost[itemIdx] = {
            state: state || null,
            do: reaction || null
        }

        return this;
    }

    hasItem

    looseItem(itemIdx) {
        let inventoryIdx = this.inventory.indexOf(itemIdx);

        if (inventoryIdx == -1) {
            return null;
        }

        this.inventory.splice(inventoryIdx, 1);

        if (typeof this.itemLost[itemIdx] !== 'undefined') {
            return {
                do: this.itemLost[itemIdx].do,
                state: this.itemLost[itemIdx].state != null ? this.itemLost[itemIdx].state : this.state
            };
        }

        return null;
    }

    onItemGot(itemIdx, state, reaction) {
        this.itemGot[itemIdx] = {
            state: state || null,
            do: reaction || null
        }

        return this;
    }

    getItem(itemIdx) {
        this.inventory.push(itemIdx);
        
        if (typeof this.itemGot[itemIdx] !== 'undefined') {
            return {
                do: this.itemGot[itemIdx].do,
                state: this.itemGot[itemIdx].state != null ? this.itemGot[itemIdx].state : this.state
            };
        }

        return null;
    }
}

class NPC extends InventorableEntity {
    constructor(name, desc, longDesc, state, inventory, dialogs) {
        super(name, desc, longDesc, state, inventory);

        if (typeof dialogs !== 'undefined' && typeof dialogs.init === 'undefined') {
            throw new Error('Dialog has no `init` state defined.');
        }
        
        this.dialogs = dialogs || {init: {say: '(Does not react.)', do: null}};
    }

    talk() {
        return (typeof this.dialogs[this.state] !== 'undefined') ? this.dialogs[this.state] : this.dialogs['init'];
    }
}

class Item extends Entity {
    constructor(name, desc, longDesc, state, movable) {
        super(name, desc, longDesc, state)

        this.movable = typeof movable !== 'undefined' ? movable : true;
    }
}

class Storage extends InventorableEntity {
    constructor(name, desc, longDesc, state, inventory, movable) {
        super(name, desc, longDesc, state, inventory);

        this.movable = typeof movable !== 'undefined' ? movable : true;
    }
}

class Location extends InventorableEntity {
    constructor(name, desc, longDesc, state, inventory, npcs, paths) {
        super(name, desc, longDesc, state, inventory);
        
        this.inventory = inventory || [];
        this.npcs = npcs || [];
        this.paths = paths || [];
    }
}

export {Player, NPC, Item, Storage, Location};