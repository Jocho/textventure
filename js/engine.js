import {terminal} from './app.js';
import {player, scenes, locations, items, npcs} from './demo-data.js';
import {Storage} from './classes.js';

let data = {
    theme: 'green',

    scene: {
        frames: null,
        action: null
    },

    player: player,
    scenes: scenes,
    locations: locations,
    items: items,
    npcs: npcs
};

class Engine {
    playerItemCheck(itemName) {
        if (!data.player.inventory.length) {
            terminal.line('Your inventory is empty.');
            return false;
        }

        if (!itemName.length) {
            terminal.line('What item do you have in mind?');
            return false;
        }

        if (!this.entityHasItem(data.player, itemName)) {
            terminal.line('You do not have `' + itemName + '` in your inventory.');
            return false;
        }

        return true;
    }

    findNpcByName(npcName) {
        let reg = new RegExp('^' + npcName + '$', 'i');

        for (let i in data.npcs) {
            if (!data.npcs.hasOwnProperty(i)) {
                continue;
            }
            if (data.npcs[i].name.match(reg)) {
                return data.npcs[i];
            }
        }

        return null;
    }

    findItemByName(itemName) {
        let reg = new RegExp('^' + itemName + '$', 'i');

        for (let i in data.items) {
            if (!data.items.hasOwnProperty(i)) {
                continue;
            }
            if (data.items[i].name.match(reg)) {
                return data.items[i];
            }
        }

        return null;
    }

    findItemIdx(item) {
        let itemIdx = -1;

        for (let i in data.items) {
            if (!data.items.hasOwnProperty(i)) {
                continue;
            }
            if (data.items[i].name == item.name) {
                itemIdx = i;
                break;
            }
        }

        return itemIdx;
    }

     exchangeItem(from, to, item) {
        let itemIdx = engine.findItemIdx(item);

        // callback list
        let cbList = [];
        
        let cbFrom = from.looseItem(itemIdx);
        let cbTo = to.getItem(itemIdx);

        let fromState = from.state;
        let toState = to.state;

        if (cbFrom != null) {
            fromState = cbFrom.state != null ? cbFrom.state : fromState;
            
            if (cbFrom.do != null)
                cbList.push(cbFrom.do);
        }

        if (cbTo != null) {
            toState = cbTo.state != null ? cbTo.state : toState;
            
            if (cbTo.do != null)
                cbList.push(cbTo.do);
        }

        let targetName = typeof to.name !== 'undefined' ? to.name : 'You';
        terminal.line(targetName + ' obtained `' + item.name + '`!');

        cbList.reduce((x, y) => {
            y(data);
            return x;
        }, null);

        from.state = fromState;
        to.state = toState;
    }

    isNpcHere(npc) {
        if (npc == null)
            return false;
            
        return data.locations[data.player.location].npcs.reduce((x, y) => {
            return x ? x : data.npcs[y].name == npc.name;
        }, false);
    }

    isItemHere(item) {
        return data.locations[data.player.location].inventory.reduce((x, y) => {
            return x ? x : data.items[y].name == item.name;
        }, false);
    }

    getEntityInventory(entity) {
        return entity.inventory.reduce((x, y) => {
            x.push(data.items[y].name);
            return x;
        }, []);
    }

    entityHasItem(entity, itemName) {
        let reg = new RegExp('^' + itemName + '$', 'i');
        
        return entity.inventory.reduce((x, y) => {
            return x || data.items[y].name.match(reg) ? true : false;
        }, false);
    }

    getPlayerOrLocationItem(itemName) {
        let item = this.findItemByName(itemName);

        if (item != null && (this.entityHasItem(data.player, itemName) || this.entityHasItem(data.locations[data.player.location], itemName))) {
            return item;
        }

        return null;
    }

    setScene(sceneName) {
        let actions = data.scenes[sceneName];

        data.scene.frames = typeof actions.say !== 'undefined' ? actions.say : [];
        data.scene.action = typeof actions.do !== 'undefined' ? actions.do : null;

        this.resolveScene(data.scene);
    }

    resolveScene(scene) {
        for (let i = 0; i < scene.frames.length; i++) {
            terminal.line(scene.frames[i]);
        }

        scene.frames = [];
        
        if (!scene.frames.length) {
            if (typeof scene.action === 'function') {
                scene.action(data);
            }

            scene.frames = null;
        }
    }

    getNpcsOfLocation(locationIdx) {
        return data
            .locations[locationIdx]
            .npcs
            .reduce((x, y) => {
                x.push(data.npcs[y].name);
                return x;
            }, []);
    }
}

let engine = new Engine();

let commands = [
    {
        command: 'paths|ways|where|routes',
        help: 'Get list of the locations you can visit from here.',
        fn: () => {
            let paths = data.locations[data.player.location].paths;
            
            if (!paths.length) {
                terminal.line('You cannot go anywhere from here.');
                return;
            }
            
            terminal.line('You can go to:');
            for (let i = 0; i < paths.length; i++) {
                terminal.line((i+1) + '. ' + data.locations[paths[i]].name);
            }
        }
    },

    {
        command: 'go|goto',
        help: 'Go to the location specified by its name. CMD> go|goto [the|a|an|into|onto] [location].',
        fn: (args) => {
            let match = args.match(/^(((to(\s+((an?)|(the)))?)|([io]nto))\s+)?(?<target>.+)$/i)
            if (!args.length) {
                terminal.line('Where do you want to go?');
                terminal.cmd('paths');
                return;
            }

            let paths = data.locations[data.player.location].paths;
            let reg = new RegExp('^' + match.groups.target + '$', 'i');

            let targetIndex = paths.reduce((x, y) => {
                return data.locations[y].name.match(reg) ? y : x;
            }, -1);

            if (targetIndex == -1) {
                terminal.line('You cannot go there.');
                return;
            }
            
            data.player.location = targetIndex;
            terminal.cmd('look');
        }
    },

    {
        command: 'look',
        help: 'Get short description of the location.',
        fn: (ev, me) => {
            terminal.line('[ ' + data.locations[data.player.location].name + ' ]');

            let location = data.locations[data.player.location];
            let desc = location.getDesc();
            desc = (desc instanceof Array) ? desc : [desc];
            desc.reduce((x, y) => {
                terminal.line(y);
            }, null);

            terminal.cmd('paths');
        }
    },

    {
        command: 'explore',
        help: 'Deeply explore current location.',
        fn: (ev, me) => {
            terminal.line('[ ' + data.locations[data.player.location].name + ' ]');

            let location = data.locations[data.player.location];
            let desc = location.getLongDesc();
            desc = (desc instanceof Array) ? desc : [desc];
            desc.reduce((x, y) => {
                terminal.line(y);
            }, null);

            terminal.cmd('paths');
        }
    },

    {
        command: 'i|inventory',
        help: 'when no target is given, open your inventory, otherwise open target\'s inventory. CMD> i [target]',
        fn: (targetName) => {
            let match = targetName.match(/^((of\s+)?(?<target>.+))?$/i);
            let inventory = [];

            if (typeof match.groups.target !== 'undefined') {
                let npc = engine.findNpcByName(match.groups.target);
                let item = engine.findItemByName(match.groups.target);
                let target;
                
                if (npc != null && engine.isNpcHere(npc)) {
                    inventory = engine.getEntityInventory(npc);
                    target = npc;
                }
                else if (item != null && engine.isItemHere(item)) {
                    if (!item instanceof Storage) {
                        terminal.line('`' + match.groups.target + '` has no storage.')
                        return;
                    }

                    inventory = engine.getEntityInventory(item);
                    target = item;
                }
                else {
                    terminal.line('`' + match.groups.target + '` is not here.');
                    return;
                }
                terminal.line('-- INVENTORY (#TARGET) --'.replace(/\#TARGET/, target.name));
            }
            else {
                inventory = engine.getEntityInventory(data.player);
                terminal.line('-- Your INVENTORY --');
            }
            
            if (!inventory.length) {
                terminal.line('(no items)');
                return;
            }
            
            inventory.reduce((x, y) => {
                terminal.line('- ' + y);
            }, []);
        }
    },

    {
        command: 'take|get',
        help: 'Obtain an item from location, NPC, or storage-object. CMD> take [item] [from] [target]',
        fn: (args) => {
            let match = args.match(/^(?<item>.+?)((\s+from)?\s+(?<target>.+))?$/i);

            if (match == null) {
                terminal.line('What do you want to take?');
                return;
            }

            let item = engine.findItemByName(match.groups.item);

            if (item != null && !item.movable) {
                terminal.line('You cannot transfer `' + item.name + '`.');
                return;
            }

            if (typeof match.groups.target !== 'undefined') {
                let npc = engine.findNpcByName(match.groups.target);
                let storage = engine.findItemByName(match.groups.target);
                
                if (npc != null && engine.isNpcHere(npc) && engine.entityHasItem(npc, match.groups.item)) {
                    engine.exchangeItem(npc, data.player, item);
                    return;
                }

                if (storage != null && engine.isItemHere(storage) && storage instanceof Storage && engine.entityHasItem(storage, match.groups.item)) {
                    engine.exchangeItem(storage, data.player, item);
                    return;
                }
                else {
                    terminal.line('`' + match.groups.target + '` does not contain `' + match.groups.item + '`.');
                }
            }
            else {
                if (engine.entityHasItem(data.locations[data.player.location], match.groups.item) && item.movable) {
                    engine.exchangeItem(data.locations[data.player.location], data.player, item);
                    return;
                }
                else {
                    terminal.line('There is no `' + match.groups.item + '` nearby.');
                }
            }
        }
    },

    {
        command: 'leave|drop',
        help: 'Leave an item from you inventory in the location. CMD> drop [item]',
        fn: (itemName) => {
            if (!engine.playerItemCheck(itemName)) {
                return;
            }

            let item = engine.findItemByName(itemName);

            engine.exchangeItem(data.player, data.locations[data.player.location], item);

            terminal.line('`' + itemName + '` was dropped.');
        }
    },

    {
        command: 'give|put',
        help: 'Put an item from player\'s inventory to the NPC or storage-item in location. CMD> give [item] [to] [target]',
        fn: (args) => {
            let match = args.match(/^(?<item>.+?)(\s+(?<where>at|to|on(to)?|in(side|to)?|over|under(neath)?)\s+(?<target>.+))?$/i);
            if (match == null) {
                terminal.line('What do you want to place?');
                return;
            }

            if (typeof match.groups.where === 'undefined' || typeof match.groups.target === 'undefined') {
                terminal.line('Where do you want to place the ' + match.groups.item + '?');
                return;
            }

            if (!engine.playerItemCheck(match.groups.item || '')) {
                return;
            }

            let item = engine.findItemByName(match.groups.item);
            let npc = engine.findNpcByName(match.groups.target);
            let storage = engine.findItemByName(match.groups.target);

            if (npc != null && engine.isNpcHere(npc)) {
                engine.exchangeItem(data.player, npc, item);
            }
            else if (storage != null && storage instanceof Storage && engine.isItemHere(storage)) {
                engine.exchangeItem(data.player, storage, item);
            }
            else {
                terminal.line('`' + match.groups.item + '` cannot be stored into `' + match.groups.target + '`.');
            }
        }
    },

    {
        command: 'use',
        help: 'When no target is given, use item on yourself or location, or use item on NPC or other item otherwise. CMD> use [item] [on] [target]',
        fn: (args) => {
            let match = args.match(/^(?<item>.+?)((\s+(on)|(to))?\s+(?<target>.+))?$/i);
            if (match == null) {
                terminal.line('What item do you want to use?');
                return;
            }

            let item = engine.findItemByName(match.groups.item);
            let entity;

            if (item == null || !engine.entityHasItem(data.player, match.groups.item)) {
                terminal.line('You don\'t have `' + match.groups.item + '`.');
                return;
            }

            if (typeof match.groups.target === 'undefined') {
                entity = data.locations[data.player.location];
            }
            else {
                let npc = engine.findNpcByName(match.groups.target);
                let item = engine.findItemByName(match.groups.target);

                if (npc != null && engine.isNpcHere(npc)) {
                    entity = npc;
                }
                else if (item != null && engine.isItemHere(item)) {
                    entity = item;
                }
                else {
                    terminal.line('Target `' + match.groups.target + '` is not here.');
                    return;
                }
            }

            let cb = entity.useItem(engine.findItemIdx(item));

            if (cb != null) {
                cb.do(data);
                entity.state = cb.state;
            }
            else {
                terminal.line('It has no effect.');
            }
        }
    },

    {
        command: 'check',
        help: 'Check NPC or item in the location or your inventory for brief description. CMD> check [target]',
        fn: (targetName) => {
            if (!targetName.length) {
                terminal.line('What do you want to check?');
                return;
            }

            let item = engine.getPlayerOrLocationItem(targetName);
            
            if (item != null) {
                terminal.line('You look at ' + item.name + ':');
                
                let desc = item.getDesc();
                desc = (desc instanceof Array) ? desc : [desc];
                desc.reduce((x, y) => {
                    terminal.line(y);
                }, null);
                
                return;
            }

            let npc = engine.findNpcByName(targetName);

            if (npc != null) {
                terminal.line('You look at ' + npc.name + ':');
                
                let desc = npc.getDesc();
                desc = (desc instanceof Array) ? desc : [desc];
                desc.reduce((x, y) => {
                    terminal.line(y);
                }, null);

                return;
            }

            terminal.line('There is no `' + targetName + '` around.');
        }
    },

    {
        command: 'analyze|inspect|examine',
        help: 'Deeply inspect object for deep description. CMD> inspect [target]',
        fn: (targetName) => {
            if (!targetName.length) {
                terminal.line('What do you want to check?');
                return;
            }

            let item = engine.getPlayerOrLocationItem(targetName);
            
            if (item != null) {
                let desc = item.getLongDesc();
                desc = (desc instanceof Array) ? desc : [desc];
                desc.reduce((x, y) => {
                    terminal.line(y);
                }, null);

                return;
            }

            let npc = engine.findNpcByName(targetName);

            if (npc != null) {
                let desc = npc.getLongDesc();
                desc = (desc instanceof Array) ? desc : [desc];
                desc.reduce((x, y) => {
                    terminal.line(y);
                }, null);

                return;
            }

            terminal.line('There is no `' + targetName + '` around.');
        }
    },

    {
        command: 'list',
        help: 'List NPCs or items. Or both when no list type is given. CMD> list [npcs|items]',
        fn: (type) => {
            if (type == '' || type == 'npcs') {
                let npcNames = engine.getNpcsOfLocation(data.player.location);
                
                if (!npcNames.length) {
                    terminal.line('Nobody is here.');
                    return;
                }

                terminal.line('-- NPCs --');

                npcNames.reduce((x, y) => {
                    terminal.line('- ' + y);
                }, []);
            }

            if (type == '' || type == 'items') {
                let itemNames = engine.getEntityInventory(data.locations[data.player.location]);
                
                if (!itemNames.length) {
                    terminal.line('There are no items available.');
                    return;
                }

                terminal.line('-- ITEMS --');

                itemNames.reduce((x, y) => {
                    terminal.line('- ' + y);
                }, []);
            }
        }
    },

    {
        command: 'talk|speak|say',
        help: 'Begin communication with a NPC.',
        fn: (args) => {
            let match = args.match(/^(to\s+(((an?)|(the))\s+)?)?(?<target>.+)$/i);

            if (typeof match.groups.target === 'undefined') {
                terminal.line('Specify who do you want to talk to. CMD> talk [to] [a|an|the] [target]');
                return;
            }

            let npc = engine.findNpcByName(match.groups.target);

            if (npc == false) {
                terminal.line('`' + match.groups.target + '` does not exist.');
                return;
            }
            
            if (!engine.isNpcHere(npc)) {
                terminal.line('`' + match.groups.target + '` is not here.');
                return;
            }

            terminal.line('-- ' + npc.name + ' --');

            let talk = npc.talk();
            
            data.scene.frames = talk.say instanceof Array ? talk.say : [talk.say];
            data.scene.action = talk.do;

            engine.resolveScene(data.scene);
        }
    }
];

export {commands, data, engine};