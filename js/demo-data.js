import {terminal} from './app.js';
import {Player, NPC, Item, Storage, Location} from './classes.js';
import { engine } from './engine.js';

let player = new Player('room', []);

let scenes = {
    menu: {
        say: ['Welcome.', 'Are you stuck?', 'Type `help` for more commands.'],
        do: () => {
            terminal.line('', 1000);
            engine.setScene('menu2');
        }
    },
    menu2: {
        say: ['Now, let us start.'],
        do: null
    },
    intro: {
        say: ['Hello', 'howdya doin', 'I am doin fine'],
        do: null
    },
    outro: {
        say: ['Bye'],
        do: null
    }
};

let locations = {
    room: new Location(
        'Room title',
        {init: 'This is a small room'},
        {init: 'This is a reall small room with no windows'},
        'init',
        ['candy'],
        ['bob'],
        ['pedestal']
    ),
    pedestal: new Location(
        'Pedestal',
        {init: 'This is a marble pedestal'},
        {init: 'The pedestal is a little bit crooked, but it is still a really majestic work of art'},
        'init',
        [],
        [],
        ['room'],
    )
};

let items = {
    knife: new Item('knife', {init: 'A small knife.'}, {init: 'A small metal knife with a sharp edge.'}, 'init'),
    candy: new Item('candy', {init: 'A candy.'}, {init: 'A small pink sweet candy.'}, 'init'),
    chest: new Storage('chest', {
            init: 'An empty chest.',
            full: 'A small chest. There is something.'
        },
        {
            init: 'This is a small wooden chest.',
            full: 'This is a wooden chest. There is something stored within it.'
        }, 'init', false, []),
};

let npcs = {
    bob: (new NPC('Bob', {init: 'He is a small dude.'}, {init: 'Bob is a really short dude. Like 5 ft or something.'}, 'init', ['knife'], {
        init: {
            say: ['Hello stranger!', 'What a beautiful day it is.', 'Are you here for something?', 'I have spare knife, let me give it to you.'],
            do: () => {
                terminal.cmd('get knife from Bob');
                npcs['bob'].state = 'noKnife';
            }
        },
        noKnife: {
            say: ['Okay, now get lost.'],
            do: null
        },
        knifeGot: {
            say: ['Hey, want the knife again? Here you go.'],
            do: () => {
                terminal.cmd('get knife from Bob');
                npcs['bob'].state = 'noKnife';
            }
        },
        dead: {
            say: ['*lies deadly*']
        }
    }))
    .onItemLost('knife', 'noKnife')
    .onItemGot('knife', 'knifeGot', () => {
        terminal.line('Yay, a knife! And it is mine!');
    })
    .onItemUsed('knife', 'dead', () => {
        ['What? Aaargh!!!', '*stab* *stab* *stab*', '*glurb* ehhh...'].reduce((x, y) => {
            terminal.line(y);
        });
    })
};

export {player, scenes, locations, items, npcs};