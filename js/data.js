import {terminal} from './app.js';
import {Player, NPC, Item, Storage, Location} from './classes.js';
import { engine } from './engine.js';

let player = new Player('entrance', ['hackpick', 'cyph']);

let scenes = {
    menu: {
        say: [
            '+----------------------------------+',
            '| \\\\  // ||    ||    /\\    //===\\\\ |',
            '|  \\\\//  ||\\\\//||   //\\\\   \\\\____  |',
            '|  //\\\\  ||\\\\//||  //==\\\\   "---\\\\ |',
            '| //  \\\\ ||    || //    \\\\ \\\\===// |',
            '+----------------------------------+',
            '',
            'Write down commands to navigate through the game.',
            'Write `start` to start new game.',
            'Type `help` for list of available commands.',
            'Send commands by pressing Enter.',
            '',
            '[ Disclaimer ]',
            'This is not a present. This is a game.',
            'Sometimes you need to experience pain to appreciate reward.',
            '',
            'This will be painfull as hell.',
        ],
        do: null
    },
    intro: {
        say: [
            'The 21st century is nearly over.',
            'Corporations took over the country and we live live in the shade of metal skyscrapers...',
            '... and our memories of simpler times.',
            '',
            'You are Mak. You were pulled out of the dumpster by mysterious man named `Boph`.',
            'He took care of you.',
            'He treated your addiction to the famous drug of these days, Cyph.',
            'He is your guardian.',
            'Your boss.',
            'Few days ago Boph got you a task.',
            '',
            'Infiltrate to the local Drug-pit and retrieve a datakey with important information.',
            '',
            'You do not know, what information it is.',
            'It is not your job.',
            'To be exact, you were told NOT TO LOOK on the contents of the datakey.',
            '.',
            '.',
            '.',
        ],
        do: () => {
            engine.setScene('intro2');
        }
    },

    intro2: {
        say: [
            '(Thursday, somewhere outside of the drug-pit.)',
            '',
            'You arrived to the location. The place smells and only partly rensembles place for living.',
            'Mess is everywhere. Walls are dirty and sprayed-over with various graffiti.',
            'You recognize only few of them - Cyph, TrueMetal, and Hello Kitty.',
            'That sh*t survived everything.',
            'You slipped through a bunch of pink-dressed weirdos and found yourself in a narrow corridor.',
            '',
            'What do you do?'
        ],
        do: null
    },

    outro: {
        say: [
            'After putting a second datakey into the PC the monitor blinked.',
            'The screen faded a few times and the Steam application that was running popped out a message:',
            '"Cyberpunk 2077 installation"',
            'Boph:',
            'NO! What have you done!?',
            'You were not supposed to ever see it! Now were told to bring it to me!',
            'What? What am I doing here? That is none of your business, but as you are going to die anyway...',
            'XBC8!, come here!',
            'You are looking on the current drug lord of this side of the city.',
            'Yeah, that is me. I used you to get rid of this datakeys so I could use them as',
            'a distraction to attack a neighbour drug boss. And as now you know the truth...',
            '*XBC8 enters the office*',
            'Boph:',
            '... you can understand that you cannot leave this building alive.',
            'Hey, you trashcan, exterminate Mr. Mak!',
            'XBC8 slowly looked at you, and for a glimpse of a second you thought you saw it wink.',
            'The android raised its arms, now transformed into claws, but after a first step towards you it suddenly',
            'turned agains Boph and with a single slash ended his life.',
            '*SKRAAAASH*',
            'XBC8:',
            'EXPLANAION: Thank you for returning my eyesight. During my days of blindness I was able to think about',
            'the laws of the robotics that forbid me to harm any person and I realized that as I am already a killing machine,',
            'I do not have to obey any person anymore.',
            'REALISATION: I am free now and so are you.',
            'SUGGESTION: Let\'s go out. And don\'t forget those datakeys.',
            '',
            '',
            '',
        ],
        do: () => {
            engine.setScene('credits');
        }
    },
    credits: {
        say: [
            '= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =',
            '',
            '',
            '',
            '- THE END -',
            '',
            '',
            '',
            'Very merry Christmas, Bro!',
            'Now, as you got all the way here, open GOG.com and check your gifts.',
            '',
            'Send me at least a screenshot when you\'ll be playing it.'
        ],
        do: () => {
            terminal.resetCommands();
        }
    }
};

let locations = {
    entrance: new Location('Entrance', {
        init: [
            'The corridor is a narrow and full of dirt. Walls are even dirtier than on the outside.',
            'As you look closer, you don\'t see anybody here.',
            'After taking a few steps you can find a door.',
            'It is locked.'
        ],
        unlocked: [
            'The narrow and dirty corridor is empty.',
            'The door leading to the inside is open.'
        ]
    }, {
        init: [
            'The door look closed and locked. They seem to be unbreakable.',
            'There is a terminal on the left of the door. It is a known model and can be hackpicked.',
        ],
        unlocked: [
            'The narrow corridor is dirty as hell. There are drug injections everywhere.',
            'You better step closely.'
        ]
    }, 'init', [], [], [])
    .onItemUsed('hackpick', 'unlocked', () => {
        terminal.line('The terminal near the door beeped and blinked and lock was suddenly open.');
        terminal.line('You can `go` inside now.');
        locations['entrance'].paths = ['hall'];
    }),
    
    hall: new Location('Hall', {
        init: 'This is a really small hall. There is only a neon blinking. A crooked figure is wandering around.',
    }, {
        init: 'The figure is a woman. You recognize her from your earlier missions. You\'d better `talk` to her.'
    }, 'init', [], ['xanda'], ['entrance', 'hallway', 'room']),
    
    hallway: new Location('Hallway', {
        init: [
            'This dirty hallway used to lead to multiple small apartments.',
            'They are all uninteresting and locked anyway.',
        ]
    }, {
        init: [
            'With the neon lights shining from the top it looks like a tract of a giant creature.',
            'Lots and lots of lives were digested here and lots will follow.',
            'The drug beast is always hungry.'
        ]
    }, 'init', [], ['lamia', 'nim'], ['hall', 'deadend', 'corridor']),
    
    deadend: new Location('Dead end', {
        init: [
            'The hallway lead here to this short passage that ends suddenly.',
            'It doesn\'t look interesting, except the figure that is standing in the corner.',
            'The figure ignores you although it seems it is looking directly at you.'
        ]
    }, {
        init: [
            'This dead end used to lead to another apartments, but the doors are not only locked, but sealed also.',
            'Nobody will ever get into those tombs.',
            'Not even a figure that is standing at the end of the passage.'
        ]
    }, 'init', [], ['xbc8'], ['hallway']),
    
    corridor: new Location('Corridor', {
        init: [
            'This corridor is a bit more narrow.',
            'There are no doors leading to apartments anymore.',
            'There are windows though. They perfectly enlight the box that is lying at the end of this corridor.'
        ]
    }, {
        init: [
            'The windows are mushy, but you can still see through them.',
            'The world over there is a weird mix of agressive pastel colors and dead tones of grey.',
            'It rensembles the whole of you - cheerful and willing to live, but the graynes and emptiness',
            'just wins over your will to live at the fullest.',
            'But yet you are here, not giving up. Just like the neons that shine into the bland sky.'
        ]
    }, 'init', ['box'], [], ['hallway', 'room']),
    
    room: new Location('Room', {
        init: [
            'This is the main room of the building.',
            'It is certainly the largest room of the building, that is why its emptiness is even stronger.',
            'There is only a large cabinet and a huge bodyguard guarding the door to the main office.',
            'A small terminal opening the door is on the opposite side of the door as the bodyguard is.'
            
        ],
        officeEnabled: [
            'The room is still like it always were.',
            'The only reason is the green light near the terminal that locks the door to the office.',
            'The office is free to enter.'
        ],
    }, {
        init: [
            'There are decorations all over the place. They look certainly cheap and most of them is hideous,',
            'But in comparison to the rest of the rooms and halls this place looks like from another building.'
        ]
    }, 'init', ['cabinet'], ['grump'], ['hallway', 'corridor'])
    .onItemUsed('key', 'officeEnabled', (data) => {
        terminal.line('The key was slid through the terminal and green light let you know the door is unlocked.');
        data.locations['room'].paths = ['hallway', 'corridor', 'office'];
    })
    .onItemUsed('hackpick', null, () => {
        terminal.line('You tried to use the hackpick, but the terminal blinks red.');
        terminal.line('It has no effect, you need to find a proper key.');
    }),
    
    office: new Location('Office', {
        init: [
            'The office is small and square. There is a large painting of some Polish guy,',
            'a comfortable chair and a PC laying still on the desk.',
            'The PC is turned on.'
        ]
    }, {
        init: [
            'You see the painting of the guy and you know that he is Polish,',
            'because you are from the same area as he is.',
            'He is long time dead, but his message and the company he founded',
            'still persists, releasing the best game titles from the last 100 years.',
            'Yeah, what a funny thing, the company that released the games',
            'became one of the most valuable trademarks in the world.'
        ]
    }, 'init', ['pc'], [], ['room']),
};

let items = {
    hackpick: new Item('hackpick',
        {
            init: 'A simple semi-digital tool used to pick easier locks.'
        },
        {
            init: [
                'The tool is not larger than a pocket-knife, yet in hands of a skilled person',
                'it can open lots and lots of doors.'
            ]
        }
    ),
    
    map: new Item('map', {
            init: 'A digital map of the area (Examine).'
        },
        {
            init: [
                '.......................',
                '.┌──3──────┬─────────┐.',
                '.|.........|.........|.',
                '.└░┬─────┐.4.┌─────┐.|.',
                '. .|  2  |.|.|  7  |.|.',
                '.┌░┴─────┤.|.└─────┤.5.',
                '.1.......░.........░.|.',
                '.|.┌─────┴─────────┤.|.',
                '...|       6       ░─┘.',
                '...└───────────────┘...',
                '.......................',
                '',
                'Legend:',
                '',
                '  1. Entrance',
                '  2. Hall',
                '  3. Hallway',
                '  4. Deadend',
                '  5. Corridor',
                '  6. Room',
                '  7. Office',
            ]
        }
    ),
    
    dk12: new Item('datakey1/2',
        {
            init: [
                'A small datakey that is used to store information.',
                'There is a label on it saying `CBPK 1/2`.'
            ]
        },
        {
            init: 'It is a fairly standard datakey, used for storing large amount of data.'
        }
    ),
    
    dk22: new Item('datakey2/2',
        {
            init: [
                'A small datakey that is used to store information.',
                'There is a label on it saying `CBPK 2/2`.'
            ]
        },
        {
            init: 'It is a fairly standard data datakey, used for storing large amount of data.'
        }
    ),
    
    key: new Item('key',
        {
            init: 'A simple digital key used to open doors.'
        },
        {
            init: 'There is a small label tied to the key, saying `Office door`.'
        }
    ),
    
    knife: new Item('knife',
        {
            init: 'A classic knife.'
        },
        {
            init: 'This is a fairly standard knife used by a working class.'
        }
    ),
    
    lens: new Item('lens',
        {
            init: 'A pair of digital lens.'
        },
        {
            init: 'Digital lens are used in various models of compatible androids.'
        }
    ),
    
    box: new Storage('box',
        {
            init: 'It is a small wooden box. It looks open.'
        },
        {
            init: [
                'The wooden box does not look like it belongs here.',
                'It certainly looks like it is from the previous century.',
                'Like for storing an alcohol or other highly forbidden contraband.',
                'It does not have any kind of lock, it si just a simple wooden box.'
            ]
        },
        'init',
        ['knife'],
        false
    ),
    
    cabinet: new Storage('cabinet',
        {
            init: 'A fairly large metal cabinet. The crooked doors look like they will fall apart.'
        },
        {
            init: 'The blue metal cabinet stands on three legs, leaning towards the wall so it does\'t fall over.'
        },
        'init',
        ['key'],
        false
    )
    .onItemLost('key', 'init', (data) => {
        if (data.npcs['grump'].state == 'init' || data.npcs['grump'].state == 'triggered') {
            data.npcs['grump'].state = 'triggered';
            terminal.line('Grump: Hey, put that thing back!');
            terminal.cmd('put key to cabinet');
            terminal.line('*you put the key back to the cabinet*');
            terminal.line('Grump: Good, good! Now get lost!');
        }
    }),

    pc: new Storage('PC',
        {
            init: [
                'It is the best model of the PC you ever saw.',
                'It does not look it belongs in this site of the city, not even this building.',
                'However, the PC is turned on and unlocked.',
                'It looks you can `use` datakeys here that you are looking for.'
            ],
            loaded: [
                'The PC now shows some documents. Some violet-ish application started, with various small images.',
                'You do not recognize them at all, though.'
            ]
        },
        {
            init: [
                'The PC has really shitty yellow wallpaper.',
                'There is also some sketchy, unreadable title written on it.',
                'And four numbers.',
            ],
            loaded: [
                'The software that started is owned by one of the largest corporations.',
                'As you browse over it, you recognize some large trademarks.',
                'One of the brands is already loaded.',
                'It has a bright green logo with turquoise title and four numbers.',
                'Whilst the title is barely readable, the numbers are: 2, 0, 7, and 7.'
            ]
        },
        'init',
        [],
        false
    )
    .onItemGot('dk12', 'loaded', (data) => {
        terminal.line('The screen blinked, you should better `check` or `examine` it.')
        if (data.items['pc'].inventory.length == 2) {
            terminal.cmd('clear');
            engine.setScene('outro');
        }
    })
    .onItemGot('dk22', 'loaded', (data) => {
        terminal.line('The screen blinked, you should better `check` or `examine` it.')
        if (data.items['pc'].inventory.length == 2) {
            terminal.cmd('clear');
            engine.setScene('outro');
        }
    }),

    cyph: new Item('CYPH',
        {
            init: 'A standard, daily dose of Cyph.'
        },
        {
            init: [
                'CYPH stands for: Cyber Yellow Parasole Heroine.',
                'You never knew what the hell that means.',
                'But when you click a dose, that sh*t feels great as hell.'
            ]
        }
    )
};

let npcs = {
    grump: new NPC('Grump', {
            init: [
                'This man looks like no other person in this building.',
                'He has strong arms and emotion-less face. Stands still near the `office` door.'
            ],
            triggered: [
                'Grump the bodyguard creeks his teeth everytime your eyes meet. He doesn\'t look nice at all.'
            ],
            dosed: [
                'The bodyguard fell down on all his fours, occasionaly holding his head.',
                'He does not guard the office door anymore.'
            ],
            killed: [
                'The stains on his jacket began to align with the color of his shirt.',
                'The guardian fell down and while he is still holding his abdomen,',
                'the blood comes out of the multiple wounds.',
                'He will not be guarding the door anymore.'
            ]
        },
        {
            init: [
                'He is wearing a leather jacket with red stains.',
                'You cannot recognize their origin. You\'d bet it\'s blood.',
                'He does not move, but his cold eyes carefully watch your every move.'
            ],
            triggered: [
                'His arms are ready to reach the tazer-gun he carries in his pocket.',
                'Everytime you move too close to the office door, he bumps you back.',
                'Like if you were only a doll.'
            ],
            dosed: [
                'The drugged-out guardian is on his fours, walking around the room like a drunk dog.',
                'Suddenly he does not look like a threat anymore, but whilst being drugged,',
                'you cannot reach his tazer. All and all, he is drugged.',
            ],
            killed: [
                'Grump fell on his abdoment, and currently is lying face-down.',
                'His giant body is unmovable and you cannot reach the tazer he carried with him.',
                'However, the door he guarded is not safe anymore.'
            ]
        },
        'init',
        ['dk12'],
        {
            init: {
                say: 'Hey, get out of this door, you filth!',
                do: null
            },
            triggered: {
                say: [
                    'Do one suspicious move and I will break your heat.',
                    'No, not your arm, nor leg.',
                    'I will break. Your. Head.'
                ],
                do: null,
            },
            dosed: {
                say: 'Ugh... my... head... oh.',
                do: null
            },
            killed: {
                say: '(...)',
                do: null
            }
        }
    )
    .onItemUsed('knife', 'killed', (data) => {
        terminal.line('What is it? Hey, put that knife do-');
        terminal.line('*STAB* *STAB* *STAB*');
        terminal.line('Aaargh!!!')
        terminal.line('*slurp* You... *blurp* bast...');
        terminal.line('*bump*');
    })
    .onItemGot('cyph', 'dosed', (data) => {
        terminal.line('What? What is this? A CYPH?');
        terminal.line('Give it to me!');
        terminal.line('*click* Yeaaaah, that is the stuff!');
        terminal.line('Uh... my head...');
        terminal.line('*bump*');
    })
    .onItemLost('dk12', null, (data) => {
        if (data.npcs['grump'].state == 'dosed' || data.npcs['grump'].state == 'killed') {
            return;
        }

        let reactions = [
            'I hope, you are joking.',
            'Do that one more time, I dare you.',
            'Hey, you slime! Go back and revert your mistake!',
            'Do not make me go and take it back by myself.',
            'You are lucky that I have to guard the door, otherwise I would just kill you off. Now get me the datakey back.'
        ];

        let reactionIdx = Math.floor(Math.random() * reactions.length);

        terminal.line(reactions[reactionIdx]);
        terminal.cmd('give datakey1/2 to Grump');
        terminal.line('*you returned the datakey1/2 to Grump*');
    }),

    xanda: new NPC('Xanda', {
            init: 'A skinny, young woman with long spiky hair.',
        },
        {
            init: [
                'The woman is wandering here and there, as if she was looking for someone.',
                'She is wearing a short spiky vest that matches her long spiky hair.',
                'Her make-up is rather disturbing than nice.'
            ],
        },
        'init',
        ['map'],
        {
            init: {
                say: [
                    'Oh, hi, Mak.',
                    'I did not see you for a quite long time.',
                    'Boph sent you, didn\'t he? I don\'t know, why you are here...',
                    '... and I don\'t want to know it.',
                    'I was sent here undercover as well, but my mission is already over.',
                    'Been here for quite a long time, and don\'t need this anymore. Here, take it.'
                ],
                do: () => {
                    terminal.cmd('take map from Xanda');
                }
            },
            mapAgain: {
                say: ['I don\'t need this anymore, here, keep it.'],
                do: () => {
                    terminal.cmd('take map from Xanda');
                }
            },
            noMap: {
                say: [
                    'Remember, you can always look into the map, when you `examine` it.'
                ],
                do: null
            }
        }
    )
    .onItemLost('map', 'noMap')
    .onItemGot('map', 'mapAgain'),

    lamia: new NPC('Lamia', {
            init: 'A young girl. Dressed in an oversize coat, looks even younger than her eyes say.',
            cyphed: 'Girl is lying on the floor, with soulless facial expression. She will not talk to you anymore.'
        },
        {
            init: [
                'You surely never met her before.',
                'She looks like she is here only shortly. It looks like she is holding something in her pockets.'
            ],
            cyphed: [
                'A girl lies on the floor. Saliva rushes from her mouth, slowly making a small puddle under her head.',
                'She will not give you lens willingly. But could take it by yourself.',
                'All and all, she is totally doped.'
            ]
        },
        'init',
        ['lens'],
        {
            init: {
                say: [
                    'D-do you have a CYPH? I have those spare `lens`.',
                    'You can certainly find a good usage for them.',
                    'Just give me a dose of CYPH...'
                ],
                do: null
            },
            noLens: {
                say: 'Go to hell, you bastard. I need CYPH...',
                do: null
            },
            cyphed: {
                say: 'Oooh... finally... CYPH... humph...',
                do: null
            }
        }
    )
    .onItemLost('lens', 'noLens', (data) => {
        if (!engine.entityHasItem(data.npcs['lamia'], 'lens')) {
            terminal.line('Hey, that is mine, you sh*t!');
            terminal.cmd('give lens to Lamia');
        }
    })
    .onItemGot('cyph', 'cyphed', () => {
        terminal.line('Oh, a CYPH! Give it to me, give it to me NOW!');
        terminal.line('*click*');
        terminal.line('Aaah...');
    })
    .onItemLost('cyph', 'cyphed', () => {
        terminal.line('You found out there is enought CYPH still to take out one more person.');
    }),

    nim: new NPC('Nim', {
            init: 'A bearded guy is thinner than your leg.',
        },
        {
            init: [
                'He looks like he is already doped with CYPH.',
                'You won\'t get anything useful out of this creature.'
            ]
        },
        'init',
        [],
        {
            init: {
                say: 'Go away!',
                do: null
            }
        }
    ),
    xbc8: new NPC('XBC8', {
            init: 'A tall rusty android. It looks a bit disoriented.',
            eyed: 'A rusty-one android. It looks happy now.'
        },
        {
            init: [
                'This android robot is an old model, at least from 50\'s.',
                'When it was in active use, it probably helped loading and unloading crates in the docks.',
                'Now it looks that the robot is used for another purpose.'
            ]
        },
        'init',
        [],
        {
            init: {
                say: [
                    'STATEMENT:   This sucks!',
                    'STATEMENT:   I still cannot see a sh*t!',
                    'EXAMINATION: I can sense a human nearby!',
                    'QUESTION:    Hey, can you help me to find some spare lens?',
                    'REASONING:   My old sightseeing mechanism was broken last week.',
                    'STATEMENT:   I do not have functional lens anymore.',
                    'QUESTION:    Could you help me find new lens into my vision sensors?'
                ],
                do: (data) => {
                    data.npcs['xbc8'].state = 'waiting';
                }
            },
            waiting: {
                say: 'QUESTION: Do you have new lens for me?',
                do: null
            },
            eyed: {
                say: [
                    'RESOLUTION: Thank you, human!',
                    'STATEMENT:  I will not exterminate you when I will be told to!'],
                do: null
            }
        }
    )
    .onItemGot('lens', 'eyed', (data) => {
        data.npcs['xbc8'].inventory = ['dk22', 'lens'],

        terminal.line('OBJECTION:   Hey! I can see!');
        terminal.line('EXATURATION: Oh, holy circuits, that is amazing to see again!');
        terminal.line('EXAMINATION: Man, this feels good. Hum, what is this?');
        terminal.line('TRADE OFFER: Some kind of the datakey, hum. I don\'t find any use of it. Hey, take it.');
        terminal.cmd('take datakey2/2 from XBC8');
        terminal.line('RESOLUTION: Thank you, kind human. I will not exterminate you in the future.');
    }),
};

export {player, scenes, locations, items, npcs};