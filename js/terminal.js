import {Elem} from '../vendor/flwjs/flw.js';
import { terminal } from './app.js';

export class Terminal {
    constructor(app, defaultCommands) {
        this.app = app;
        this.stack = [];
        this.history = [];
        this.historyPointer = -1;
        
        this.checkStack();
        
        this.commands = [];
        this.commandList = {};
        this.commandNames = [];
        
        this.textSpeed = 3;
        this.writeLn = null;
        this.writeTs = null;

        this.defaultCommands = defaultCommands || [];

        this.loadCommands(defaultCommands || []);
    }

    checkStack() {
        window.requestAnimationFrame(() => {
            if (this.stack.length || this.writeLn !== null) {
                if (this.writeLn == null) {
                    this.writeLn = this.stack.shift();
                    this.writeTs = new Date().getTime();

                    if (null !== this.writeLn.props.buffer) {
                        let content = this.app.get('screen.content');
                        content.push(this.writeLn);
                        this.app.set('screen.content', content);
                    }
                }

                if (
                    this.writeLn.props.type === 'text'
                    && null !== this.writeLn.props.buffer
                    && this.writeLn.props.buffer.length
                ) {
                    this.write();
                }
                else if (this.canWrite()) {
                    this.writeLn.renderer.update();
                    if (this.writeLn.props.type === 'command') {
                        this.runCommand(this.writeLn.props.line)
                    }
                    this.writeLn = null;
                }

                window.scrollTo(0, document.body.scrollHeight);
            }
            
            this.checkStack();
        });
    }

    commandHistory() {
        let hist = this.history.slice(-11);

        return hist.slice(0, hist.length - 1);
    }

    runCommand(text) {
        if (typeof text == 'undefined')
            return;

        text = text.trim();

        if(!text.length)
            return;

        let commandString = this.commandNames.join(')|(');
        let reg = new RegExp('^(?<command>(' + commandString + '))(\\s+(?<arguments>(.*)))?$', 'i');
        let match = text.match(reg);

        if (match == null) {
            terminal.line('Unknown command `' + text + '`.');
            return;
        }

        let commandIndex = this.commandList[match.groups.command];

        if (typeof commandIndex !== 'undefined') {
            this.commands[commandIndex].fn(match.groups.arguments || '');
        }

        this.app.renderer.update();
    }

    line(text, delay) {
        this.stack.push(new Elem('line', {
            source: '<',
            buffer: text,
            line: '',
            delay: Number.isInteger(delay) ? delay : 60,
            type: 'text',
            invokeClass: 'description'
        }));
    }

    cmd(command, delay) {
        this.stack.push(new Elem('line', {
            source: '>',
            line: command,
            buffer: '',
            delay: Number.isInteger(delay) ? delay : 0,
            type: 'command',
            invokeClass: 'command',
        }));
    }

    write() {
        if (null === this.writeLn.props.buffer || !this.writeLn.props.delay || !this.textSpeed) {
            this.writeLn.props.line = this.writeLn.props.buffer;
            this.writeLn.props.buffer = '';
        }
        else {
            if (this.canWrite()) {
                this.writeTs = new Date().getTime();

                this.writeLn.set('line', this.writeLn.props.line += this.writeLn.props.buffer.slice(0, 1));
                this.writeLn.props.buffer = this.writeLn.props.buffer.slice(1);
            };
        }
    }

    canWrite() {
        return !this.textSpeed || new Date().getTime() >= (this.writeTs + this.writeLn.props.delay / this.textSpeed);
    }

    loadCommands(commands) {
        this.commands = this.commands.concat(commands);

        this.commandNames = [];
        this.commandList = {};
        
        for (let i = 0; i < this.commands.length; i++) {
            this.commands[i].command.split('|').reduce((x, y) => {
                this.commandNames.push(y);
                this.commandList[y] = i;
            }, null);
        }
    }

    resetCommands() {
        this.commands = [];
        this.loadCommands(this.defaultCommands);
    }
}
