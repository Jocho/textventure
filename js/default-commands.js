import { terminal } from './app.js';
import {commands, data, engine} from './engine.js';

export let defaultCommands = [
    {
        command: 'clear',
        help: 'Clears whole screen, leaving you more space for your commands.',
        fn: (words) => {
            terminal.app.set('screen.content', []);
        }
    },
    
    {
        command: 'history',
        help: 'Shows ten latest commands you used.',
        fn: (words) => {
            terminal.app.set('screen.content', []);
            terminal.line('-- HISTORY --');

            let hist = terminal.commandHistory();
            for (let i = 0; i < hist.length; i++) {
                terminal.line((new String(i+1)).padStart(hist.length, ' ') + '. ' + hist[i]);
            }
        }
    },
    
    {
        command: 'help',
        help: 'Shows list of available commands.',
        fn: (words) => {
            terminal.line('-- HELP --');

            let span = terminal.commands.reduce((x, y) => {
                x = x < y.command.length ? y.command.length : x;
                return x;
            }, 0);

            span += 1;

            terminal.commands.reduce((x, y) => {
                terminal.line(y.command.padEnd(span, ' ') + '- ' + y.help);
                return x;
            }, null);    
        }
    },
    
    {
        command: 'start',
        help: 'Starts new game.',
        fn: () => {
            terminal.loadCommands(commands);
            terminal.app.set('screen.content', []);
            engine.setScene('intro');
        }
    },

    {
        command: 'load',
        help: 'Loads saved state.',
        fn: () => {
            terminal.line('Loading...');
            terminal.loadCommands(commands);

            let save = window.localStorage.getItem('savegame');

            if (save == null) {
                terminal.line('No save found.');
                return;
            }

            save = JSON.parse(save);

            data.theme = save.theme;

            terminal.app.set('theme', data.theme);
            terminal.textSpeed = save.textSpeed || 1;

            data.player.location = save.player.location;
            data.player.inventory = save.player.inventory;

            let dataParts = ['locations', 'npcs', 'items'];
            
            for (let i = 0; i < dataParts.length; i++) {
                for (let j in save[dataParts[i]]) {
                    if (!save[dataParts[i]].hasOwnProperty(j)) {
                        continue;
                    }

                    for (let k in save[dataParts[i]][j]) {
                        if (!save[dataParts[i]][j].hasOwnProperty(k))
                            continue;
                        
                        data[dataParts[i]][j][k] = save[dataParts[i]][j][k];
                    }
                }
            }

            terminal.app.set('screen.content', []);
        }
    },

    {
        command: 'save',
        help: 'Saves current progress.',
        fn: () => {
            terminal.line('Saving...');

            // locations
            let locations = [];
            for (let i in data.locations) {
                if (!data.locations.hasOwnProperty(i)) {
                    continue;
                }

                locations[i] = {
                    state: data.locations[i].state,
                    inventory: data.locations[i].inventory,
                    npcs: data.locations[i].npcs,
                    paths: data.locations[i].paths
                };
            }

            // npcs
            let npcs = [];

            for (let i in data.npcs) {
                if (!data.npcs.hasOwnProperty(i)) {
                    continue;
                }

                npcs[i] = {
                    state: data.npcs[i].state,
                    inventory: data.npcs[i].inventory
                };
            }

            // items
            let items = [];

            for (let i in data.items) {
                if (!data.items.hasOwnProperty(i)) {
                    continue;
                }

                items[i] = data.items[i] instanceof Storage
                    ? {
                        state: data.items[i].state,
                        inventory: data.items[i].inventory
                    }
                    : {
                        state: data.items[i].state
                    };
            }
            
            let save = {
                textSpeed: terminal.textSpeed || 1,
                theme: data.theme,

                player: {
                    location: data.player.location,
                    inventory: data.player.inventory,
                },
                locations: locations,
                items: items,
                npcs: npcs
            };

            window.localStorage.setItem('savegame', JSON.stringify(save));

            terminal.line('Your progress was saved...');
        }
    },

    {
        command: 'reset',
        help: 'Remove all the saves and reset the state.',
        fn: () => {
            window.localStorage.removeItem('savegame');
            terminal.app.set('screen.content', []);
            engine.setScene('menu');
        }
    },

    {
        command: 'ts|textspeed',
        help: 'Show or set the speed of the displayed text. CMD> textspeed [slow|normal|fast|instant]',
        fn: (textSpeed) => {
            let speeds = {
                slow: 1,
                normal: 3,
                fast: 6,
                instant: 0
            }

            if (!textSpeed.length) {
                for (let i in speeds) {
                    if (!speeds.hasOwnProperty(i))
                        continue;
                    if (speeds[i] == terminal.textSpeed) {
                        terminal.line('Current text speed is set to `' + i + '`.');
                        break;
                    }
                }

                return;
            }

            if (['slow', 'normal', 'fast', 'instant'].indexOf(textSpeed) > -1) {
                terminal.textSpeed = speeds[textSpeed];
            }
            else {
                terminal.line('Choose from acceptable text speed values: slow, normal, fast, instant.');
            }
        }
    },

    {
        command: 'theme',
        help: 'Change theme between gray, turquoise, green, blue, and red.',
        fn: (themeName) => {
            if (!themeName.length) {
                terminal.line('What color do you want to use? (gray|turquoise|green|blue|red).');
                return;
            }

            switch (themeName) {
                case 'gray':
                case 'grey':
                case 'turquoise':
                case 'green':
                case 'red':
                case 'blue':
                    terminal.app.set('theme', themeName);
                    data.theme = themeName;
                    break;

                default:
                    terminal.line('Please, use valid color theme: gray|turquoise|green|blue|red.');
                    break;
            }
        }
    }
];
