
class Swiper {
    constructor() {
        this.swipeResolver = new SwipeResolver();
        this.swipedArea = {
            width: 0,
            height: 0
        };
    }

    start(ev, me) {
        this.swipedArea = {
            width: parseInt(window.getComputedStyle(ev.target.parentElement).width.replace(/(\d+).*/, '$1')),
            height: parseInt(window.getComputedStyle(ev.target.parentElement).height.replace(/(\d+).*/, '$1'))
        };

        this.swipeResolver.start(ev);
    }

    continue(ev, me, trackedVar, limit) {
        this.swipeResolver.process(ev);

        if (!swiper.swipeResolver.isSwiping)
            return;

        if (swiper.swipeResolver.distanceX > this.swipedArea.width / limit) {
            let trv = parseInt(me.get('data.' + trackedVar));

            switch (this.swipeResolver.direction) {
                case this.swipeResolver.DIR_LEFT:
                    trv--;
                    break;
                case this.swipeResolver.DIR_RIGHT:
                    trv++;
                    break;
            }

            if (trv < 0)
                trv = 0;

            if (trv > limit)
                trv = limit;

            swiper.swipeResolver.x = swiper.swipeResolver.lastX;
            swiper.swipeResolver.y = swiper.swipeResolver.lastY;

            me.set(trackedVar, (100 * trv) / limit);
            me.set('data.' + trackedVar, trv);
        }
    }

    end(ev) {
        this.swipeResolver.end(ev);
    }
}

class SwipeResolver {
    constructor(sensitivity) {
        this.sensitivity = parseInt(sensitivity) || 0;
        this.DIR_LEFT = 'left';
        this.DIR_RIGHT = 'right';
        this.DIR_UP = 'up';
        this.DIR_DOWN = 'down';

        this.x = 0;
        this.y = 0;
        this.lastX = 0;
        this.lastY = 0;

        this.reset();
    };

    reset() {
        this.swipeInvoked = false;
        this.isSwiping = false;
        this.direction = null;
        this.distanceX = 0;
        this.distanceY = 0;
        this.distance = 0;
    };

    start(ev) {
        if (ev instanceof TouchEvent) {
            this.x = ev.touches[0].clientX;
            this.y = ev.touches[0].clientY;
        }
        else {
            this.x = ev.x;
            this.y = ev.y;
        }

        this.reset();

        this.swipeInvoked = true;
    };

    process(ev) {
        if (!this.swipeInvoked)
            return;

        let x, y;
        if (ev instanceof TouchEvent) {
            x = ev.touches.length ? ev.touches[0].clientX : this.lastX;
            y = ev.touches.length ? ev.touches[0].clientY : this.lastY;
        }
        else {
            x = ev.x;
            y = ev.y;
        }
        this.lastX = x;
        this.lastY = y;

        this.distanceX = Math.abs(this.x - x);
        this.distanceY = Math.abs(this.y - y);

        this.distance = Math.sqrt(Math.pow(this.distanceX, 2) + Math.pow(this.distanceY, 2));

        if (this.distance > this.sensitivity) {
            this.isSwiping = true;
        }

        if (this.distanceX > this.distanceY) {
            this.direction = this.x > x ? this.DIR_LEFT : this.DIR_RIGHT;
        }
        else {
            this.direction = this.y > y ? this.DIR_UP : this.DIR_DOWN;
        }
    };

    end(ev) {
        if (this.isSwiping) {
            this.isSwiping = false;
            this.swipeInvoked = false;
            return {
                direction: this.direction,
                distanceX: this.distanceX,
                distanceY: this.distanceY,
                distance: this.distance
            };
        }
        else {
            this.reset();
        }
    };
}
